from textblob.classifiers import NaiveBayesClassifier
import nltk.metrics
from nltk.metrics.scores import precision
from nltk import precision
import pickle


#load training set from file
emergencyFile = open('emergencyTweet', 'r')
non_emergencyFile = open('non-emergencyTweet', 'r')

emergency = [ line.decode('utf-8').strip() for line in emergencyFile.readlines()]
non_emergency = [line.decode('utf-8').strip() for line in non_emergencyFile.readlines()]


#label training set: emergency or non-emergency
train = []
for line in emergency:
		t = (line, 'emergency')
		train.insert(0,t)
for line in non_emergency:
		t = (line, 'non-emergency')
		train.insert(0,t)

emergencyFile.close()
non_emergencyFile.close()

#load test set from file
emergencyFileTest = open('emergencyTweet_test', 'r')
non_emergencyFileTest = open('non-emergencyTweet_test', 'r')

emergencyTest = [ line.decode('utf-8').strip() for line in emergencyFileTest.readlines()]
non_emergencyTest = [line.decode('utf-8').strip() for line in non_emergencyFileTest.readlines()]

#lable test set: emergency or non-emergency
test = []  #test is a list of data
for line in emergencyTest:
	t = (line, 'emergency')
	test.insert(0,t)
for line in non_emergencyTest:
	t = (line, 'non-emergency')
	test.insert(0,t)

#######add some code
emergencyTestData = []
for line in emergencyTest:
	t = (line, 'emergency')
	emergencyTestData.insert(0,t)

nonemergencyTestData = []
for line in non_emergencyTest:
	t = (line, 'non-emergency')
	nonemergencyTestData.insert(0,t)
########end of this block

#train and test
cl = NaiveBayesClassifier(train)
#save the trained model to local file
classifierFile = open("trained_model.pickle", 'wb')
pickle.dump(cl, classifierFile)
classifierFile.close();


#test the model
result = cl.accuracy(test)
print 'accuraty: '
print result
most_informative_features = cl.show_informative_features(15)
print most_informative_features

#adding more code below:
emergencyResult = cl.accuracy(emergencyTestData)
print 'emergency test result accuracy is: '
print emergencyResult
#emrePrecision = nltk.metrics.scores.precision(train['emergency'], emergencyTest['emergency'])

nonemerResult = cl.accuracy(nonemergencyTestData)
print 'non-emergency test result accuracy is '
print nonemerResult
