from google.cloud import language
from google.cloud.language import enums
from google.cloud.language import types


def entities_text(text):
    """Detects entities in the text."""
    client = language.LanguageServiceClient()

    # Instantiates a plain text document.
    document = types.Document(
        content=text,
        type=enums.Document.Type.PLAIN_TEXT
        #content="hello"
        )

    # Detects entities in the document. You can also analyze HTML with:
    #   document.type == enums.Document.Type.HTML
    entities = client.analyze_entities(document).entities
    #print entities
    # entity types from enums.Entity.Type
    entity_type = ('UNKNOWN', 'PERSON', 'LOCATION', 'ORGANIZATION',
                   'EVENT', 'WORK_OF_ART', 'CONSUMER_GOOD', 'OTHER')

    #for entity in entities:
    #    print('=' * 20)
    #    print(u'{:<16}: {}'.format('name', entity.name))
    #    print(u'{:<16}: {}'.format('type', entity_type[entity.type]))
    #    print(u'{:<16}: {}'.format('salience', entity.salience))
    content = []
    for entity in entities:
        content.append(entity.name.encode('utf-8'))
        #if entity_type == 'LOCATION':
        #    print('=' * 20)
        #    print(u'{:<16}: {}'.format('name', entity.name))
        #    #result.write(u'{:<16}: {}'.format('name', entity.name).encode("utf-8"))
        #    print(u'{:<16}: {}'.format('type', entity_type[entity.type]))
        #    print(u'{:<16}: {}'.format('salience', entity.salience))
        #print content
    str = ' '.join(content)
    print str
    return str

with open('emergencyTweet_test', 'r') as file:
    lines = file.readlines()
#doc = file.read()

#news = [ line.decode('utf-8').strip() for line in file.readlines()]
for line in lines:
    entities_text(line)
    #print news
    #print entities_text(line)
file.close()

print 'finished'

#print text
