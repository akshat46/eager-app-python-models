import retrive_tweets
import schedule
import time

#https://stackoverflow.com/questions/373335/how-do-i-get-a-cron-like-scheduler-in-python
def job():
    execfile('retrive_tweets')
    print "i am working"

schedule.every(1).minutes.do(job)

while True:
    schedule.run_pending()
    time.sleep(1)
